Name:           python-jaraco-context
Version:        5.1.0
Release:        1
Summary:        Tools to work with functools
License:        MIT
URL:            https://pypi.org/project/jaraco.context
Source0:        https://files.pythonhosted.org/packages/source/j/jaraco.context/jaraco.context-%{version}.tar.gz

BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-setuptools
BuildRequires:  python3-toml
BuildRequires:  python3-pip python3-wheel

BuildArch:      noarch

%description
jaraco.functools Tools for working with functools.
Additional functools in the spirit of stdlib’s functools.

%package -n python3-jaraco-context
Summary:        %{Summary}

%description -n python3-jaraco-context
jaraco.functools Tools for working with functools.
Additional functools in the spirit of stdlib’s functools.

%prep
%autosetup -n jaraco.context-%{version} -p1

%build
%pyproject_build

%install
%pyproject_install

%check
pytest

%files -n python3-jaraco-context
%license LICENSE
%doc docs/*.rst README.rst
%{python3_sitelib}/jaraco/__pycache__/*
%{python3_sitelib}/jaraco/context.py*
%{python3_sitelib}/jaraco.context-%{version}*-info

%changelog
* Thu Aug 15 2024 yaoxin <yao_xin001@hoperun.com> - 5.1.0-1
- Update to 5.1.0:
  * Implement experimental _compose for composing context
    managers. If you wish to use this function, please comment in
    the issue regarding your thoughts on the ordering.
  * Deprecate null context.
  * Renamed tarball_context to tarball and deprecated tarball_context compatibility shim. 
  * Disentangle pushd from tarball.
  * Removed deprecated 'runner' parameter to tarball_context.

* Fri Apr 28 2023 yaoxin <yao_xin001@hoperun.com> - 4.3.0-1
- Package init
